package com.rvcode.playground.task6.yurii;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class Deque<Item> implements Iterable<Item> {

    private Node listFront;
    private Node listEnd;
    private int size;

    private class Node {
        Item item;
        Node next;
        Node prev;
    }

    private class DequeIterator implements Iterator<Item> {
        private Node current = listFront;

        @Override
        public boolean hasNext() {
            return current != null;
        }

        @Override
        public Item next() {
            if (current == null){
                throw new NoSuchElementException();
            }
            Item item = current.item;
            current = current.next;
            return item;
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }

    }

    public Deque() {                          // construct an empty deque
        listEnd = null;
        listFront = null;
        size = 0;
    }

    public boolean isEmpty() {                // is the deque empty?
        return listFront == null;
    }

    public int size() {                        // return the number of items on the deque
        return size;
    }

    public void addFirst(Item item) {         // insert the item at the front
        if (item == null) {
            throw new NullPointerException();
        }
        if (isEmpty()) {
            listFront = new Node();
            listEnd = listFront;
            listFront.item = item;
        } else {
            Node newFront = new Node();
            newFront.item = item;
            newFront.next = listFront;
            listFront.prev = newFront;
            listFront = newFront;
        }
        size++;
    }
    public void addLast(Item item) {           // insert the item at the end
        if (item == null) {
            throw new NullPointerException();
        }
        if (isEmpty()) {
            listEnd = new Node();
            listFront = listEnd;
            listEnd.item = item;
        } else {
            Node newEnd = new Node();
            newEnd.item = item;
            listEnd.next = newEnd;
            newEnd.prev = listEnd;
            listEnd = newEnd;
        }
        size++;
    }
    public Item removeFirst() {               // delete and return the item at the front
        if (isEmpty()){
            throw new NoSuchElementException();
        }
        Item removeItem = listFront.item;
        listFront = listFront.next;
        size--;
        return removeItem;
    }
    public Item removeLast() {                 // delete and return the item at the end
        if (isEmpty()){
            throw new NoSuchElementException();
        }
        Item removeItem = listEnd.item;
        listEnd = listEnd.prev;
        listEnd.next.prev = null;
        listEnd.next = null;
        size--;
        return removeItem;
    }
    public Iterator<Item> iterator() {        // return an iterator over items in order from front to end
        return new DequeIterator();
    }

    public void insertByIndex(int index, Item item){
        if (index >= size-1 && index < 0) {
            throw new NoSuchElementException();
        }
        if (index == size-1){
            addLast(item);
        } else {
            int i = 0;
            Node current = listFront;
            Node newNode = new Node();
            newNode.item = item;
            while (current != null){
                if (i == index){
                    newNode.next = current.next;
                    current.next.prev = newNode;
                    current.next = newNode;
                    newNode.prev = current;
                    size++;
                }
                current = current.next;
                i++;
            }
        }
    }

    public void removeByIndex(int index){
        if (index >= size-1 && index < 0) {
            throw new NoSuchElementException();
        }
        int i = 0;
        Node current = listFront;
        if (index == 0){
            removeFirst();
        } else
        if (index == size -1){
            removeLast();
        } else {
            while (current != null){
                if (i == index){
                    current.prev.next = current.next;
                    current.next.prev = current.prev;
                    current.next = null;
                    current.prev = null;
                    size--;
                }
                current = current.next;
                i++;
            }
        }

    }

    public void removeByValue(Item item){
        if (item == null) {
            throw new NullPointerException();
        }
        Node current = listFront;
        int i = 0;
        while (current != null) {
            if (current.item.equals(item)){
                if (i == size-1){
                    removeLast();
                } else {
                    if ( i == 0) {
                        removeFirst();
                    } else {
                        current.prev.next = current.next;
                        current.next.prev = current.prev;
                        current.next = null;
                        current.prev = null;
                        size--;
                    }
                }
            }
            i++;
            current = current.next;
        }
    }

    public int firstIn(Item item){
        Node current = listFront;
        int i = 0;
        while (current != null){
            if (current.equals(item)){
                System.out.println("AM");
            }
            i++;
            current = current.next;
        }
        return i;
    }

    public void replaceByIndex(int index, Item item){
        if (index >= size-1 && index < 0) {
            throw new NoSuchElementException();
        }
        if (item == null) {
            throw new NullPointerException();
        }

        int i = 0;
        Node current = listFront;
        while (current != null){
            if (i == index){
                current.item = item;
                break;
            }
            current = current.next;
            i++;
        }
    }

    public void reverse(){
        Node current = listEnd.prev;
        Node first = listEnd;
        Node second = listEnd.prev;
        while (current != null){
            second = current;
            first.next = second;
            second.prev = first;
            current = current.prev;
            first = second;
        }
        listEnd = listFront;
        listFront = current;
    }
}

