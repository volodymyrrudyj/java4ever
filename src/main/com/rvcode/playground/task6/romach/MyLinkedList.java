package com.rvcode.playground.task6.romach;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Реализовать структуру данных - связный список целых чисел, которая должна
 * поддерживать следующие операции:

 * - вставка в конец
 * - вставка в начало
 * - вставка после заданного индекса
 * - удаление по индексу
 * - удаление по значению (всех элементов с заданным значением)
 * - поиск первого вхождения элемента
 * - замена элемента по индексу указанным значением
 * - “разворот” списка (т.е. [1, 2, 3, 4, 5] превращается в [5, 4, 3, 2, 1])
 * @param <T> Type of the list
 */
public class MyLinkedList<T> implements Iterable<T> {
    private Node<T> firstNode;
    private int size = 0;

    /**
     * Create empty LinkedList
     */
    public MyLinkedList(){

    }

    /**
     * Create LinkedList with values of integers
     * @param values array of integers
     */
    public MyLinkedList(T... values){
        for (T value : values) {
            this.add(value);
        }
    }

    /**
     * Returns value of list element with defined specified index
     * @param index index of returned element
     * @return value of element
     * @throws IndexOutOfBoundsException
     */
    public T get(int index) throws IndexOutOfBoundsException {
        final Node<T> node = this.getNode(index);
        return node.getValue();
    }

    /**
     * Add value to the end of the List
     * @param value Node value
     */
    public void add(final T value){
        if (this.size == 0) {
            this.firstNode = new Node<>(value);
        } else {
            final Node node = this.getNode(size - 1);
            node.nextNode = new Node<>(value);
        }
        this.size++;
    }

    /**
     * Insert value after specified index
     * @param index Index after witch to insert value
     * @param value Value of inserted element
     */
    public void addAfter(int index, final T value) {
        checkIndex(index);
        final Node node = new Node<>(value);
        final Node prevNode = this.getNode(index);
        node.nextNode = prevNode.nextNode;
        prevNode.nextNode = node;
        this.size++;
    }

    /**
     * Add value to beginning of the list
     * @param value Node value
     */
    public void addFirst(final T value){
        this.firstNode = new Node<>(value, this.firstNode);
        this.size++;
    }

    /**
     * Delete value with specified index
     * @param index Index of deleting Node
     * @throws IndexOutOfBoundsException
     */
    public void deleteIndex(int index) throws IndexOutOfBoundsException{
        this.checkIndex(index);
        if (index == 0)  {
            this.firstNode = this.firstNode.nextNode;
        } else if (index == this.size - 1) {
            this.getNode(index - 1).setNextNode(null);
        } else {
            this.getNode(index - 1).setNextNode(this.getNode(index + 1));
        }
        size--;
    }

    /**
     * Delete ALL Nodes with specified values
     * @param value Value of deleting nodes
     */
    public void deleteValues(final T value){
        int currentNodeIndex = 0;
        for (Node currentNode = this.firstNode; currentNode != null; currentNode = currentNode.getNextNode() ){
            if (currentNode.getValue() == value) {
                deleteIndex(currentNodeIndex);
                currentNodeIndex--;
            }
            currentNodeIndex++;
        }
    }

    /**
     * Get index of node with specified value
     * @param value Searched node value
     * @return index of found node (-1 if not found )
     */
    public int indexOf(final T value){
        int foundIndex = -1;
        int currentIndex = -1;
        for (Node currentNode = this.firstNode; currentNode != null; currentNode = currentNode.getNextNode() ){
            currentIndex++;
            if (currentNode.getValue() == value) {
                foundIndex = currentIndex;
                break;
            }
        }
        return foundIndex;
    }

    /**
     * Set value to index position
     * @param index Index of Node
     * @param value Setting value
     * @throws IndexOutOfBoundsException
     */
    public void set(int index, final T value) throws IndexOutOfBoundsException{
        this.getNode(index).setValue(value);
    }

    /**
     * 1 2 3 4 5 -> 5 4 3 2 1
     */
    public void reverse() {
        final MyLinkedList<T> reversedList = new MyLinkedList<>();
        for (Node<T> currentNode = this.firstNode; currentNode != null; currentNode = currentNode.getNextNode() ){
            reversedList.addFirst((currentNode.getValue()));
        }
        this.firstNode = reversedList.firstNode;
    }

    /**
     * Check index for out of bounds
     * @param index Specified index
     * @throws IndexOutOfBoundsException
     */
    private void checkIndex(int index) throws IndexOutOfBoundsException{
        if ((index < 0) || (index >= size)){
            throw new IndexOutOfBoundsException();
        }
    }

    /**
     * Return node with specified index
     * @param index Index of Node
     * @return Searched node
     */
    private Node<T> getNode(int index) {
        this.checkIndex(index);
        Node<T> node = this.firstNode;

        for (int i = 0; i < index; i++) {
            node = node.getNextNode();
        }
        return node;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        final MyLinkedList that = (MyLinkedList) o;

        if (size != that.size) return false;

        Iterator thisIterator = this.iterator();
        Iterator thatIterator = that.iterator();

        while (thisIterator.hasNext()) {
            if (!(thisIterator.next().equals(thatIterator.next()))) {
                return false;
            }
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = 0;
        for (Object o : this) {
            result += o.hashCode();
        }
        result = 31 * result + size;
        return result;
    }

    /**
     * Returns an iterator over a set of elements of type T.
     *
     * @return an Iterator.
     */
    @Override
    public Iterator<T> iterator() {
        return new MyIterator();
    }

    private class MyIterator implements Iterator<T> {
        private Node<T> node = MyLinkedList.this.firstNode;
        /**
         * Returns {@code true} if the iteration has more elements.
         * (In other words, returns {@code true} if {@link #next} would
         * return an element rather than throwing an exception.)
         *
         * @return {@code true} if the iteration has more elements
         */
        @Override
        public boolean hasNext() {
            return this.node != null;
        }

        /**
         * Returns the next element in the iteration.
         *
         * @return the next element in the iteration
         * @throws NoSuchElementException if the iteration has no more elements
         */
        @Override
        public T next(){
            if (this.hasNext()) {
                final Node<T> previousNode = this.node;
                this.node = this.node.nextNode;
                return previousNode.getValue();
            }
            return null;
        }

        /**
         * Unsupported operation
         */
        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

    /**
     * Incapsulates value and next Node
     */
    private static class Node<Type>{
        private Type value;
        private Node<Type> nextNode;

        /**
         * Create Node with Value. NextNode = null
         * @param value Node's value
         */
        private Node(final Type value){
            this.value = value;
        }

        /**
         * Create Node with Value and NextNode
         * @param value Node's value
         * @param nextNode Next node
         */
        private Node(final Type value, final Node<Type> nextNode) {
            this(value);
            this.nextNode = nextNode;
        }

        /**
         * Get Value of the Node
         * @return Value of the Node
         */
        private Type getValue() {
            return value;
        }

        /**
         * Set value of the Node
         * @param value Node's value
         */
        private void setValue(final Type value) {
            this.value = value;
        }

        /**
         * Get Next Node
         * @return Next Node
         */
        private Node<Type> getNextNode() {
            return this.nextNode;
        }

        /**
         * Set Next Node
         * @param nextNode Next Node
         */
        private void setNextNode(final Node<Type> nextNode) {
            this.nextNode = nextNode;
        }
    }
}