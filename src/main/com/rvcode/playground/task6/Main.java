package com.rvcode.playground.task6;

import com.rvcode.playground.task6.lisnychyi.MyConcurrentLinkedList;
import com.rvcode.playground.task6.tanya.MyList;
import com.rvcode.playground.task6.yurii.Deque;
import com.rvcode.playground.task6.vladNakon.List;

import java.lang.Double;
import java.lang.Integer;
import java.lang.String;
import java.lang.System;
import java.util.Iterator;

public class Main {
    public static void main(String[] args) {
        System.out.println("Add your code to the main method");
        demoRomach();
        demoVladNakon();
        demoGwinblade();
        demoTanya();
        yuriisEx();
        demoSerhii();
    }

    private static void demoVladNakon() {
        //vladNakon
        //com.vladNakon.task6.List<Integer> list = new com.vladNakon.task6.List<Integer> ();
        System.out.println("*******************\nvladNakon list:\n");
        List<Integer> list = new List<>();
        list.addBack(8);
        list.addBack(3);
        list.addBack(55);
        list.addBack(12);
        list.addBack(18);

        //com.vladNakon.task6.Iterator<Integer> it = list.iterator();
        //while(it.hasNext()){
        //    System.out.println(it.next());
        //}
        //End
    }

    private static void demoRomach() {
        // Romach LinkedList start
        System.out.println("*******************");
        System.out.println("Romach Linked List is working:");
        com.rvcode.playground.task6.romach.MyLinkedList<String> romachList = new com.rvcode.playground.task6.romach.MyLinkedList<>("One", "Two", "Three");
        System.out.println("Elements of the list are:");
        for (String s : romachList) {
            System.out.println(s);
        }

        // Romach LinkedList end
    }

    private static void demoGwinblade() {
        //gwinblade linked list start
        System.out.println("*******************\nGwinblade list:\n");
        com.rvcode.playground.task6.gwinblade.MyList<Double> gwinList = new com.rvcode.playground.task6.gwinblade.MyList<>();
        gwinList.addEnd(-65.124);
        gwinList.addEnd(541.3);
        gwinList.addEnd(3.14);
        gwinList.addBegin(2.71);
        for (Double element : gwinList) {
            System.out.println(element);
        }
        //gwinblade linkedl list end
    }


    private static void demoTanya() {
        System.out.println("*******************\nTanya list:\n");
        final MyList<Integer> myList = new MyList<Integer>();
        myList.append(100);
        myList.append(200);
        myList.append(300);
        myList.append(400);

        for (final Integer i : myList) {
            System.out.println("Elements of the list are: " + i);
        }
    }

    private static void yuriisEx() {
    	  System.out.println("*******************\nyurii list:\n");
        Deque<Integer> myList = new Deque<>();
        myList.addFirst(34);
        myList.addFirst(5465);
        myList.addLast(7346);
        myList.addLast(75);
        for (Integer integer : myList) {
            System.out.println(integer);
        }
    }

    private static void demoSerhii() {
    	  System.out.println("*******************\nSerhii list:\n");
        MyConcurrentLinkedList<Integer> myList = new MyConcurrentLinkedList<>();
        myList.add(1);
        myList.add(2);
        myList.add(3);
        for(Integer value : myList){
            System.out.println(value);
        }
        System.out.println("\nSlisnychyi List: " + myList);
    }


}