package com.rvcode.playground.task6.gwinblade;
import java.util.*;

public class MyList<T> implements Iterable<T>
{
    private class Node
    {
        T element;
        Node next;
    }
    private Node begin;
    private Node end;
    private int size;

    public boolean isEmpty()
    {
        return size == 0;
    }

    public int size()
    {
        return size;
    }

    public void addEnd(T element)
    {
        Node node = new Node();
        node.element = element;
        if (isEmpty()) {
            begin = end = node;
        }
        else {
            end.next = node;
            end = node;
        }
        size++;
    }

    public void addBegin(T element)
    {
        Node node = new Node();
        node.element = element;
        if (isEmpty()) {
            begin = end = node;
        }
        else {
            node.next = begin;
            begin = node;
        }
        size++;
    }

    public boolean addAfter(int index, T element)
    {
        if (isEmpty() || index < 0 || index > size - 1) {
            return false;
        }
        Node node = new Node();
        node.element = element;
        Node iterator = begin;
        for (int i = 0; i < index; i++) {
            iterator = iterator.next;
        }
        node.next = iterator.next;
        iterator.next = node;
        size++;
        return true;
    }

    public boolean remove(int index)
    {
        if (isEmpty() || index < 0 || index > size - 1) {
            return false;
        }
        if (index == 0) {
            Node temp = begin;
            begin = begin.next;
            temp.next = null;
        }
        else {
            Node iterator = begin;
            for (int i = 0; i < index - 1; i++) {
                iterator = iterator.next;
            }
            if (index == size - 1) {
                end = iterator;
            }
            Node temp = iterator.next;
            iterator.next = iterator.next.next;
            temp.next = null;
        }
        size--;
        return true;
    }

    public void removeAll(T element)
    {
        Node iterator = begin;
        while (iterator != null) {
            int index = -1;
            for (int i = 0; i < size; i++) {
                if (iterator != null) {
                    if (iterator.element.equals(element)) {
                        index = i;
                        break;
                    }
                    iterator = iterator.next;
                }
            }
            if (index != -1) {
                remove(index);
                iterator = begin;
            }
        }
    }

    public int find(T element)
    {
        Node iterator = begin;
        for (int i = 0; i < size; i++) {
            if (iterator.element.equals(element)) {
                return i;
            }
            iterator = iterator.next;
        }
        return -1;
    }

    public boolean set(int index, T element)
    {
        if (isEmpty() || index < 0 || index > size - 1) {
            return false;
        }
        Node node = new Node();
        node.element = element;
        Node iterator = begin;
        if (index == 0) {
            Node temp = begin;
            begin = node;
            begin.next = iterator.next;
            temp.next = null;
        }
        else {
            for (int i = 0; i < index - 1; i++) {
                iterator = iterator.next;
            }
            if (index == size - 1) {
                end = node;
            }
            Node temp = iterator.next;
            iterator.next = node;
            node.next = temp.next;
            temp = null;
        }
        return true;
    }

    public void reverse()
    {
        Node iterator = begin;
        for (int i = 0; i < size; i++) {
            Node node = new Node();
            node.element = iterator.element;
            if (i == 0) {
                end = node;
            }
            else {
                node.next = begin;
            }
            begin = node;
            iterator = iterator.next;
        }
    }

    public boolean arrrayEquals(T[] values)
    {
        if (size == 0 || size - values.length != 0) {
            return false;
        }
        int count = 0;
        for (Node i = begin; i != null; i = i.next) {
            if (!values[count++].equals(i.element)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public Iterator iterator()
    {
        return new ListIterator();
    }

    private class ListIterator implements Iterator
    {
        private Node iterator;
        private int count;
        private int current;

        @Override
        public T next()
        {
            count++;
            if (count > size) {
                throw new NoSuchElementException();
            }
            if (count == 1) {
                iterator = begin;
                return iterator.element;
            }
            iterator = iterator.next;
            return iterator.element;
        }

        @Override
        public boolean hasNext()
        {
            return count >= size ? false : true;
        }

        @Override
        public void remove()
        {
            if (count == 0 || count == current) {
                throw new IllegalStateException();
            }
            if (count == 1) {
                begin = iterator.next;
                iterator.next = null;
                size--;
                return;
            }
            final Node next = iterator.next;
            Node priv = begin;
            for (int i = 0; i < count - 2; i++) {
                priv = begin.next;
            }
            priv.next = next;
            iterator.next = null;
            iterator = priv;
            if (count == size) {
                end = priv;
            }
            size--;
            current = count;
        }
    }
}
