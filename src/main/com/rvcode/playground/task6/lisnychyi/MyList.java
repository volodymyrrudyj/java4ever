package com.rvcode.playground.task6.lisnychyi;


import static com.rvcode.playground.task6.lisnychyi.MyConcurrentLinkedList.Node;

public interface MyList<T> {
    //functional methods
    void addFirst(T value);
    void add(T value);
    void addByIndex(T value, int index);
    void removeByIndex(int index);
    void removeByValue(T value);
    int findElement(T value);
    void replace(T value, int index);
    void reverse();
    //service method
    int size();
    Node getFirst();
    Node getLast();
    void setLast(Node last);
}
