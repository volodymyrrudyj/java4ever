package com.rvcode.playground.task6.lisnychyi;

import java.util.Iterator;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class MyConcurrentLinkedList<T> implements MyList<T>, Iterable<T> {

    private final ReentrantReadWriteLock lock = new ReentrantReadWriteLock();
    private final Lock rLock = lock.readLock();
    private final Lock wLock = lock.writeLock();

    private Node first;
    private Node last;
    private AtomicInteger size = new AtomicInteger(0);

    private volatile int counter;

    public MyConcurrentLinkedList() {
    }
    public MyConcurrentLinkedList(MyConcurrentLinkedList<T> list) {
        addAll(list);
    }
    public int size() {
        try {
            rLock.lock();
            return size.get();
        } finally {
            rLock.unlock();
        }
    }
    public Node getFirst() {
        try {
            rLock.lock();
            return first;
        } finally {
            rLock.unlock();
        }
    }
    public Node getLast() {
        if(last!=null){
            return last;
        }
        return first;
    }
    public void setLast(Node last) {
        try {
            wLock.lock();
            this.last = last;
        } finally {
            wLock.unlock();
        }
    }

    private void addAll(MyConcurrentLinkedList<T> list) {
        if (list.size() > 0) {
           this.first = list.getFirst();
           this.last = list.getLast();
           this.size = list.size;
        }
    }

    @SuppressWarnings("unchecked")
    public T getByIndex(int index){
        if(index > size() || index < 0){counter = 0; throw new IndexOutOfBoundsException();}
        Node next = this.first.getNext();
        int c = 0;
        while (c != index){
            next = next.getNext();
            c++;
        }
        return (T) next.getValue();
    }

    @Override
    @SuppressWarnings("unchecked")
    public void addFirst(T value) {
        if (value != null) {
            try {
                wLock.lock();
                if (size.get() == 0) {
                    size.incrementAndGet();
                }
                Node firstNode = this.getFirst();
                if (firstNode != null) {
                    this.first = new Node(firstNode, value);
                }      else {
                    this.first = new Node(null, value);
                }

            } finally {
                wLock.unlock();
            }
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public void add(T value) {
        if (value != null) {
            try {
                wLock.lock();
                if (size.get() == 0) {
                    addFirst(value);
                }
                else{
                    size.incrementAndGet();
                        Node prev = this.getLast();
                        Node last = new Node(null, value);
                        prev.setNext(last);
                        this.last = last;
                }
            } finally {
                wLock.unlock();
            }
        }
    }

    @Override
    public void addByIndex(T value, int index) {
        if(index > size.get() || index < 0){counter = 0; throw new IndexOutOfBoundsException();}
        Node next = first;
        while (counter != index){
            next = next.getNext();
            counter++;
        }
        counter = 0;
        size.incrementAndGet();
        next.setNext(new Node<T>(next.getNext(), value));
    }

    @Override
    @SuppressWarnings("unchecked")
    public void removeByIndex(int index) {
        if(index > size.get() || index < 0){counter = 0; throw new IndexOutOfBoundsException();}
        if(index == counter) {first = first.getNext(); size.decrementAndGet(); return;}
        Node prev = first;
        while(counter != index-1){
            prev = prev.getNext();
            counter++;
        }
        counter = 0;
        size.decrementAndGet();
        Node nextInQueue = prev.getNext();
        Node nextAfterNext = nextInQueue.getNext();
        Node nextAfterNextAfterNext = nextInQueue.getNext().getNext();
        //nextInQueue = null;
        prev.setNext(new Node(nextAfterNextAfterNext, nextAfterNext.getValue()));
    }
    @Override
    public void removeByValue(T value) {
        Node node = first;
        while(!node.equals(last)){
            if(value.equals(node.getValue())){
                removeByIndex(counter);
                counter++;
            }
            node = node.getNext();
        }
        counter = 0;
    }

    @Override
    public int findElement(T value) {
        Node node = first;
        while(!node.equals(last)){
            if(value.equals(node.getValue())){
                break;
            }
            counter++;
            node = node.getNext();
        }
        int result = counter;
        counter = 0;
        return result;
    }

    @Override
    public void replace(T value, int index) {
        addByIndex(value, index);
        removeByIndex(index);
    }

    @Override
    @SuppressWarnings("unchecked")
    public void reverse() {
        Node firstReverse = new Node(null, last.getValue());
        Node lastReverse = null;
        for(int i = size.get()-1;i>0;i--){
            Node newNode = new Node(null , getNodeByIndex(i-1).getValue());
            if(i == size.get()-1){
                firstReverse.setNext(newNode);
                lastReverse = newNode;
            }
            else{
                lastReverse.setNext(newNode);
                lastReverse = newNode;
            }
        }
        this.first = firstReverse;
        this.last = lastReverse;
    }

    private Node getNodeByIndex(int index){
        if(index > size.get() || index < 0){counter = 0; throw new IndexOutOfBoundsException();}
        Node node = first;
        while(counter != index){
            node = node.getNext();
            counter++;
        }
        counter = 0;
        return node;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MyConcurrentLinkedList that = (MyConcurrentLinkedList) o;

        if (counter != that.counter) return false;
        if (first != null ? !first.equals(that.first) : that.first != null) return false;
        if (last != null ? !last.equals(that.last) : that.last != null) return false;
        if (size != null ? size.equals(that.size) : that.size != null) return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = first != null ? first.hashCode() : 0;
        result = 31 * result + (last != null ? last.hashCode() : 0);
        result = 31 * result + (size != null ? size.hashCode() : 0);
        result = 31 * result + counter;
        return result;
    }
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(" ");
        Node next = first;
        if(next != null){
            while (next != null){
                Integer value = (Integer) next.getValue();
                builder.append(value).append(" ");
                next = next.getNext();
            }
        }
        return "MyConcurrentLinkedList [" + builder.toString() + "]";
    }

    public static class Node<T> {
        private Node next;
        private T value;

        private Node(Node next, T value) {
            this.next = next;
            this.value = value;
        }

        public Node getNext() {
            return next;
        }

        public void setNext(Node next) {
            this.next = next;
        }

        public T getValue() {
            return value;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Node node = (Node) o;

            if (next != null ? !next.equals(node.next) : node.next != null) return false;
            if (value != null ? !value.equals(node.value) : node.value != null) return false;

            return true;
        }
        @Override
        public int hashCode() {
            int result = next != null ? next.hashCode() : 0;
            result = 31 * result + (value != null ? value.hashCode() : 0);
            return result;
        }
    }
    @Override
    public Iterator<T> iterator() {
        return new MyConcurrentLinkedListIterator<T>();
    }
    public class MyConcurrentLinkedListIterator<E> implements Iterator<E> {

        @Override
        public boolean hasNext(){
            if(counter < size.get()){
                return true;
            }
            else{
                counter = 0;
                return false;
            }
        }

        @Override
        public E next(){
            return getByIndex(counter++);
        }

        @Override
        public void remove() {
        }

        @SuppressWarnings("unchecked")
        public E getByIndex(int index){
            if(index > size.get() || index < 0){counter = 0; throw new IndexOutOfBoundsException();}
            Node next = first;
            for(int i = 0; i!=index; i++){
                next = next.getNext();
            }
            return (E) next.getValue();
        }
    }

}
