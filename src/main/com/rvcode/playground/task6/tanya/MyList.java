package com.rvcode.playground.task6.tanya;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class MyList<T> implements Iterable<T>{

	private Node<T> head;
	private int length;
	
	public MyList(){
		this.head = null;
		this.length = 0;
	}
	
	public T get(int index){
		Node<T> temp = new Node<T>();
		temp = head;
		for (int i=0; i<index;i++){
			temp = temp.getNext();
		}
		return temp.getValue();
	}
	

	public void prepend(T value){
		Node<T> newNode = new Node<T>();
		newNode.setValue(value);
		newNode.setNext(head);
		head = newNode;
		length++;
	}
	
	
	public void append(T value){
		Node<T> newNode = new Node<T>();
		newNode.setValue(value);
		Node<T> temp = head;
		if (head == null){
			prepend(value);
			length++;
		} else{
		
			while(temp.getNext() != null){
				temp = temp.getNext();
			}
			temp.setNext(newNode);
			length++;
		}
		
	}
	
	public void addAfterIndex(T value, int index){
		Node<T> newNode = new Node<T>();
		newNode.setValue(value);
		Node<T> prev = null;
		Node<T> curr = head;
		for(int i=0; i < index; i++){
			prev = curr;
			curr = curr.getNext();
		}
		if (prev == null){
			prepend(value);
			length++;
		} else {
			newNode.setNext(curr);
			prev.setNext(newNode);
			length++;
		}
	}
	
	public void removeElementByIndex(int index){
		Node<T> prev = null;
		Node<T> curr = head;
		for(int i=0; i < index; i++){
			prev = curr;
			curr = curr.getNext();
		}
		if(prev == null){
			head = head.getNext();
			length--;
		} else{
			prev.setNext(curr.getNext());
			length--;
		}
	}
	
	public int findElement(T value){
		int index = -1; 
		
		if (head != null){
			for(Node<T> current = head; current.getNext() != null; current = current.getNext()){
				if (current.getValue().equals(value)){
					break;
				}
				index++;
				
			}
		}
		return index;
		
	}
	
	public void removeElementByValue(T value){
		Node<T> prev = null;
		if(head != null){
			for(Node<T> current = head;current.getNext() != null; current = current.getNext()){
				if (current.getValue().equals(value)){
					if (prev != null){
						prev.setNext(current.getNext());
					} else{
						head = head.getNext();
					}
				}
				prev = current;
			}
		}
	}	
	
	public void replaceElement(int index, T value){
		removeElementByIndex(index);
		addAfterIndex(value, index);
	}
	
	public void reversal(){
		Node<T> current;
		MyList<T> myList = new MyList<>();
		
		if(head != null){
		
			for(current = head;current != null; current = current.getNext()){
				myList.prepend(current.getValue());
			}
			
		}
		this.head = myList.head;
	}

	@Override
	public Iterator<T> iterator() {
			Iterator<T> iter = new Iterator<T>(){

				private Node<T> current = head;
				
				@Override
				public boolean hasNext() {
					return (current != null);
				}
				

				@Override
				public T next() {
					if(current != null){
						T value = current.getValue();
						current = current.getNext();
						return value;
					}	
					else{
						throw new NoSuchElementException();
					}
				}

				@Override
				public void remove() {
					// TODO Auto-generated method stub	
				}
				
			};
			return iter;
	}
}
