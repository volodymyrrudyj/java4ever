package com.rvcode.playground.task6.gwinblade;

import java.util.*;
import org.junit.Test;
import static org.junit.Assert.*;

public class MyListTest
{
    @Test
    public void testAddEnd()
    {
        final MyList<Integer> list = new MyList<>();
        assertFalse(list.arrrayEquals(new Integer[]{1}));
        list.addEnd(1);
        assertTrue(list.arrrayEquals(new Integer[]{1}));
        list.addEnd(6);
        list.addEnd(15);
        assertTrue(list.arrrayEquals(new Integer[]{1, 6, 15}));
        list.addEnd(777);
        assertFalse(list.arrrayEquals(new Integer[]{1, 6, 15, 776}));
        assertTrue(list.arrrayEquals(new Integer[]{1, 6, 15, 777}));
        list.addEnd(0);
        assertTrue(list.arrrayEquals(new Integer[]{1, 6, 15, 777, 0}));
        list.addEnd(-5);
        assertTrue(list.arrrayEquals(new Integer[]{1, 6, 15, 777, 0, -5}));
        assertFalse(list.arrrayEquals(new Integer[]{0, 6, 15, 776}));
    }

    @Test
    public void testAddBegin()
    {
        final MyList<Integer> list = new MyList<>();
        list.addBegin(94);
        assertTrue(list.arrrayEquals(new Integer[]{94}));
        list.addEnd(8);
        list.addEnd(-21);
        assertTrue(list.arrrayEquals(new Integer[]{94, 8, -21}));
        list.addEnd(671);
        list.addBegin(0);
        assertTrue(list.arrrayEquals(new Integer[]{0, 94, 8, -21, 671}));
        list.addBegin(-84);
        assertTrue(list.arrrayEquals(new Integer[]{-84, 0, 94, 8, -21, 671}));
        list.addBegin(567);
        list.addBegin(-32);
        assertTrue(list.arrrayEquals(new Integer[]{-32, 567, -84, 0, 94, 8, -21, 671}));
    }

    @Test
    public void testAddAfter()
    {
        final MyList<Integer> list = new MyList<>();
        list.addEnd(5);
        list.addEnd(13);
        list.addEnd(-6);
        list.addEnd(4);
        list.addEnd(-91);
        list.addAfter(0, 777);
        assertTrue(list.arrrayEquals(new Integer[]{5, 777, 13, -6, 4, -91}));
        list.addAfter(5, 777);
        assertTrue(list.arrrayEquals(new Integer[]{5, 777, 13, -6, 4, -91, 777}));
        list.addAfter(3, 0);
        assertTrue(list.arrrayEquals(new Integer[]{5, 777, 13, -6, 0, 4, -91, 777}));
        assertFalse(list.addAfter(51, 777));
        assertFalse(list.addAfter(-1, 777));
        list.addAfter(1, 999);
        assertTrue(list.arrrayEquals(new Integer[]{5, 777, 999, 13, -6, 0, 4, -91, 777}));
    }

    @Test
    public void testRemove()
    {
        final MyList<Integer> list = new MyList<>();
        list.addEnd(5);
        list.addEnd(13);
        list.addEnd(-6);
        list.addEnd(4);
        list.addEnd(-91);
        list.remove(0);
        assertTrue(list.arrrayEquals(new Integer[]{13, -6, 4, -91}));
        list.remove(3);
        assertTrue(list.arrrayEquals(new Integer[]{13, -6, 4}));
        list.addEnd(0);
        assertFalse(list.remove(-12));
        assertFalse(list.remove(32));
        list.remove(2);
        assertTrue(list.arrrayEquals(new Integer[]{13, -6, 0}));
    }

    @Test
    public void testRemoveAll()
    {
        final MyList<Integer> list = new MyList<>();
        list.addEnd(56);
        list.addEnd(31);
        list.addEnd(-7);
        list.addEnd(56);
        list.addEnd(5);
        list.addEnd(56);
        list.addEnd(5);
        list.addEnd(-3);
        list.addEnd(56);
        list.removeAll(56);
        assertTrue(list.arrrayEquals(new Integer[]{31, -7, 5, 5, -3}));
        list.removeAll(5);
        assertTrue(list.arrrayEquals(new Integer[]{31, -7, -3}));
        list.addEnd(12);
        list.addEnd(12);
        list.addEnd(12);
        list.addEnd(12);
        list.removeAll(12);
        assertTrue(list.arrrayEquals(new Integer[]{31, -7, -3}));
        list.removeAll(-7);
        assertTrue(list.arrrayEquals(new Integer[]{31, -3}));
    }

    @Test
    public void testFind()
    {
        final MyList<Integer> list = new MyList<>();
        list.addEnd(32);
        list.addEnd(12);
        list.addEnd(-7);
        list.addEnd(32);
        list.addEnd(1);
        list.addEnd(0);
        list.addEnd(42);
        list.addEnd(-3);
        list.addEnd(59);
        assertEquals(0, list.find(32));
        assertEquals(5, list.find(0));
        assertEquals(7, list.find(-3));
        assertEquals(2, list.find(-7));
        assertEquals(6, list.find(42));
    }

    @Test
    public void testSet()
    {
        final MyList<Integer> list = new MyList<>();
        list.addEnd(8);
        list.addEnd(-2);
        list.addEnd(10);
        list.addEnd(-48);
        list.addEnd(123);
        list.addEnd(10);
        list.addEnd(54);
        list.addEnd(-91);
        list.addEnd(591);
        list.set(0, 777);
        assertTrue(list.arrrayEquals(new Integer[]{777, -2, 10, -48, 123, 10, 54, -91, 591}));
        list.set(5, 0);
        assertTrue(list.arrrayEquals(new Integer[]{777, -2, 10, -48, 123, 0, 54, -91, 591}));
        list.set(3, 123);
        assertTrue(list.arrrayEquals(new Integer[]{777, -2, 10, 123, 123, 0, 54, -91, 591}));
        list.set(8, 18);
        assertTrue(list.arrrayEquals(new Integer[]{777, -2, 10, 123, 123, 0, 54, -91, 18}));
        assertFalse(list.set(-3, 12));
        assertFalse(list.set(12, 22));
    }

    @Test
    public void testReverse()
    {
        final MyList<Integer> list1 = new MyList<>();
        list1.addEnd(321);
        list1.addEnd(-212);
        list1.addEnd(17);
        list1.addEnd(0);
        list1.addEnd(34);
        list1.addEnd(-6);
        list1.addEnd(33);
        list1.addEnd(-932);
        list1.addEnd(54);
        list1.reverse();
        assertTrue(list1.arrrayEquals(new Integer[]{54, -932, 33, -6, 34, 0, 17, -212, 321}));
        final MyList<Integer> list2 = new MyList<>();
        list2.addEnd(0);
        list2.addEnd(1);
        list2.addEnd(2);
        list2.addEnd(3);
        list2.addEnd(4);
        list2.addEnd(5);
        list2.addEnd(6);
        list2.addEnd(7);
        list2.addEnd(8);
        list2.reverse();
        assertTrue(list2.arrrayEquals(new Integer[]{8, 7, 6, 5, 4, 3, 2, 1, 0}));
    }

    @Test(expected = NoSuchElementException.class)
    public void testIteratorNoSuchElementException()
    {
        final MyList<Integer> list = new MyList<>();
        final Iterator<Integer> iterator = list.iterator();
        iterator.next();
    }

    @Test(expected = IllegalStateException.class)
    public void testIteratorIllegalStateException()
    {
        final MyList<Integer> list = new MyList<>();
        final Iterator<Integer> iterator = list.iterator();
        iterator.remove();
    }

    @Test(expected = IllegalStateException.class)
    public void complexIteratorTest()
    {
        final MyList<Integer> list = new MyList<>();
        final Iterator<Integer> iterator = list.iterator();
        assertFalse(iterator.hasNext());
        list.addEnd(18);
        list.addEnd(-32);
        list.addEnd(140);
        list.addEnd(1448);
        list.addEnd(431);
        list.addEnd(304);
        list.addEnd(-49);
        list.addEnd(-61);
        list.addEnd(512);
        assertTrue(iterator.hasNext());
        final Integer[] array1 = new Integer[9];
        int i = 0;
        for (Integer element : list) {
            array1[i++] = element;
        }
        assertArrayEquals(array1, new Integer[]{18, -32, 140, 1448, 431, 304, -49, -61, 512});
        iterator.next();
        iterator.next();
        iterator.next();
        iterator.remove();
           
        final Integer[] array2 = new Integer[8];
        i = 0;
        for (Integer element : list) {
            array2[i++] = element;
        }     
        assertArrayEquals(array2, new Integer[]{18, -32, 1448, 431, 304, -49, -61, 512});
        iterator.next();
        iterator.remove();
        assertTrue(list.arrrayEquals(new Integer[]{18, -32, 431, 304, -49, -61, 512}));
        iterator.remove();
    }
}
