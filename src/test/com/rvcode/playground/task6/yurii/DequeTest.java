package com.rvcode.playground.task6.yurii;

import org.junit.Assert;
import org.junit.Test;
import com.rvcode.playground.task6.yurii.Deque;

public class DequeTest {
    @Test
    public void testOne(){
        System.out.println("Hello World!!!");
        Deque<Integer> firstDeque = new Deque<Integer>();
        firstDeque.addFirst(new Integer(5));
        firstDeque.removeFirst();
        Assert.assertTrue(firstDeque.isEmpty());
    }

    @Test
    public void testTwo(){
        System.out.println("Hello World!!!");
        Deque<Integer> firstDeque = new Deque<Integer>();
        firstDeque.addFirst(new Integer(5));
        firstDeque.addFirst(new Integer(455));
        Assert.assertEquals("Size ", 2, firstDeque.size());
    }
}
