package com.rvcode.playground.task6.vladNakon.task6;
import static org.junit.Assert.*;
import junit.framework.Assert;

import com.rvcode.playground.task6.vladNakon.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import java.*;


public class testList {
	

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}
	//private List list;
	
	@Before
	public void setUp() throws Exception {
		//list = new List();	
		
	}
	
	@After
	public void tearDown() throws Exception {
	}
	String sFront = " 11 8 10";
	
	@Test
	public void testAddFront() {
		List list = new List();
		list.addBack(8);
		list.addBack(10);
		Assert.assertEquals(sFront, list.addFront(11));
	}

	String sReplaceElement = " 8 11 5 3";
	@Test
	public void testReplaceElement() {
		List list = new List();
		list.addBack(8);
		list.addBack(10);
		list.addBack(5);
		list.addBack(3);
		Assert.assertEquals(sReplaceElement, list.replaceElement(2, 11));
		
	}

	String sBack = " 8 10 11";
	@Test
	public void testAddBack() {
		List list = new List();
		list.addBack(8);
		list.addBack(10);
		Assert.assertEquals(sBack, list.addBack(11));
	}

	String sRemoveIndex = " 8 10 3";
	@Test
	public void testRemoveIndex() {
		List list = new List();
		list.addBack(8);
		list.addBack(10);
		list.addBack(5);
		list.addBack(3);
		Assert.assertEquals(sRemoveIndex, list.removeIndex(3));
	}
	
	String sRemoveValue = " 10 5 3";
	@Test
	public void testRemoveValue() {
		List list = new List();
		list.addBack(8);
		list.addBack(10);
		list.addBack(5);
		list.addBack(8);
		list.addBack(3);
		list.addBack(8);
		Assert.assertEquals(sRemoveValue, list.removeValue(8));
	}

	int FirstValue = 2;
	@Test
	public void testFirstValue() {
		List list = new List();
		list.addBack(8);
		list.addBack(10);
		list.addBack(5);
		list.addBack(3);
		Assert.assertEquals(FirstValue, list.firstValue(10));
	}

	String reverse = " 3 5 10 8";
	@Test
	public void testReverse() {
		List list = new List();
		list.addBack(8);
		list.addBack(10);
		list.addBack(5);
		list.addBack(3);
		Assert.assertEquals(reverse, list.reverse());
	}
	
	String addAfter = " 8 10 11 5 8 3";
	@Test
	public void testAddAfterIndex() {
		List list = new List();
		list.addBack(8);
		list.addBack(10);
		list.addBack(5);
		list.addBack(8);
		list.addBack(3);
		list.addBack(8);
		Assert.assertEquals(addAfter, list.addAfterIndex(3, 11));
	}
}
