package com.rvcode.playground.task6.lisnychyi;

import com.rvcode.playground.task6.lisnychyi.MyConcurrentLinkedList;
import com.rvcode.playground.task6.lisnychyi.MyList;
import org.junit.Assert;
import org.junit.Test;

public class MyConcurrentLinkedListTest {
    @Test
    public void testAddFirst() throws Exception {
        //input value
        int value = 1;
        //init class
        MyList<Integer> list = new MyConcurrentLinkedList<>();
        //test method call
        list.addFirst(value);
        //expected result
        //assert
        Assert.assertEquals(1, list.getFirst().getValue());
    }
    @Test
    public void testPutFirst_withNull() throws Exception {
        //input value
        //init class
        MyList<Integer> list = new MyConcurrentLinkedList<>();
        //test method call
        list.addFirst(null);
        //expected result
        //assert
        Assert.assertNull(list.getFirst());
    }
    @Test
    public void testPutLast() throws Exception {
        //input value
        Integer value = 454;
        MyConcurrentLinkedList<Integer> internalList = new MyConcurrentLinkedList<>();
        internalList.add(1);
        internalList.add(2);
        internalList.add(3);
        System.out.println(internalList);
        //init class
        MyList<Integer> list = new MyConcurrentLinkedList<>(internalList);
        //test method call
        list.add(value);
        //expected result
        Integer result = (Integer) list.getLast().getValue();
        //assert
        Assert.assertEquals(result, value);
    }
    @Test
    public void testPutFirst() throws Exception {
        //input value
        Integer value = 454;
        //init class
        MyList<Integer> list = new MyConcurrentLinkedList<>();
        //test method call
        list.add(value);
        //expected result
        Integer result = (Integer) list.getFirst().getValue();
        //assert
        Assert.assertEquals(result, value);
    }

    @Test
    public void testPutByIndex() throws Exception {
        //input value
        Integer value = 454;
        MyConcurrentLinkedList<Integer> internalList = new MyConcurrentLinkedList<>();
        internalList.add(1);
        internalList.add(2);
        internalList.add(3);
        //System.out.println(internalList);
        //init class
        MyList<Integer> list = new MyConcurrentLinkedList<>(internalList);
        //test method call
        list.addByIndex(value, 1);
        //expected result
        MyConcurrentLinkedList<Integer> expectedList = new MyConcurrentLinkedList<>();
        expectedList.add(1);
        expectedList.add(2);
        expectedList.add(value);
        expectedList.add(3);

        System.out.println(list + "\n" + expectedList);
        //assert
        Assert.assertEquals(expectedList, list);
    }

    @Test
    public void testRemoveByIndex() throws Exception {
        //input value
        MyConcurrentLinkedList<Integer> internalList = new MyConcurrentLinkedList<>();
        internalList.add(1);
        internalList.add(2);
        internalList.add(3);
        //System.out.println(internalList);
        //init class
        MyList<Integer> list = new MyConcurrentLinkedList<>(internalList);
        //test method call
        list.removeByIndex(1);
        //expected result
        MyConcurrentLinkedList<Integer> expectedList = new MyConcurrentLinkedList<>();
        expectedList.add(1);
        expectedList.add(3);
        System.out.println(list + "\n" + expectedList);
        //assert
        Assert.assertEquals(expectedList, list);
    }
    @Test
    public void testRemoveByIndexFirst() throws Exception {
        //input value
        MyConcurrentLinkedList<Integer> internalList = new MyConcurrentLinkedList<>();
        internalList.add(1);
        internalList.add(2);
        internalList.add(3);
        //System.out.println(internalList);
        //init class
        MyList<Integer> list = new MyConcurrentLinkedList<>(internalList);
        //test method call
        list.removeByIndex(0);
        //expected result
        MyConcurrentLinkedList<Integer> expectedList = new MyConcurrentLinkedList<>();
        expectedList.add(2);
        expectedList.add(3);
        System.out.println(list + "\n" + expectedList);
        //assert
        Assert.assertEquals(expectedList, list);
    }

    @Test
    public void testRemoveByValue() throws Exception {
        //input value
        MyConcurrentLinkedList<Integer> internalList = new MyConcurrentLinkedList<>();
        internalList.add(1);
        internalList.add(1);
        internalList.add(3);
        internalList.add(3);
        internalList.add(4);
        internalList.add(5);
        //System.out.println(internalList);
        //init class
        MyList<Integer> list = new MyConcurrentLinkedList<>(internalList);
        //test method call
        list.removeByValue(1);
        list.removeByValue(3);
        //expected result
        MyConcurrentLinkedList<Integer> expectedList = new MyConcurrentLinkedList<>();
        expectedList.add(4);
        expectedList.add(5);
        System.out.println(list + "\n" + expectedList);
        //assert
        Assert.assertEquals(expectedList, list);
    }

    @Test
    public void testFindElement() throws Exception {
        //input value
        MyConcurrentLinkedList<Integer> internalList = new MyConcurrentLinkedList<>();
        internalList.add(1);
        internalList.add(1);
        internalList.add(3);
        internalList.add(3);
        internalList.add(4);
        internalList.add(5);
        //System.out.println(internalList);
        //init class
        MyList<Integer> list = new MyConcurrentLinkedList<>(internalList);
        //test method call
        int elemIndex = list.findElement(5);
        //expected result
        int expectedIndex = 5;
        //assert
        Assert.assertEquals(expectedIndex, elemIndex);
    }

    @Test
    public void testReplace() throws Exception {
        //input value
        MyConcurrentLinkedList<Integer> internalList = new MyConcurrentLinkedList<>();
        internalList.add(1);
        internalList.add(1);
        internalList.add(3);
        internalList.add(3);
        internalList.add(4);
        internalList.add(5);
        //System.out.println(internalList);
        //init class
        MyList<Integer> list = new MyConcurrentLinkedList<>(internalList);
        //test method call
        list.replace(999, 2);
        //expected result
        MyConcurrentLinkedList<Integer> expectedList = new MyConcurrentLinkedList<>();
        expectedList.add(1);
        expectedList.add(1);
        expectedList.add(999);
        expectedList.add(3);
        expectedList.add(4);
        expectedList.add(5);
        System.out.println(list + "\n" + expectedList);
        //assert
        Assert.assertEquals(expectedList, list);
    }

    @Test
    public void testReverse() throws Exception {
        //input value
        MyConcurrentLinkedList<Integer> internalList = new MyConcurrentLinkedList<>();
        internalList.add(1);
        internalList.add(2);
        internalList.add(3);
        //System.out.println(internalList);
        //init class
        MyList<Integer> list = new MyConcurrentLinkedList<>(internalList);
        //test method call
        list.reverse();
        //expected result
        MyConcurrentLinkedList<Integer> expectedList = new MyConcurrentLinkedList<>();
        expectedList.add(3);
        expectedList.add(2);
        expectedList.add(1);
        System.out.println(list + "\n" + expectedList);
        //assert
        Assert.assertEquals(expectedList, list);
    }
}
