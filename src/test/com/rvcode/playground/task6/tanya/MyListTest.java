package com.rvcode.playground.task6.tanya;
import static org.junit.Assert.assertEquals;

import org.junit.Test;


public class MyListTest {
	
	@Test
	public void testPrepend(){
	
		MyList<Integer> list = new MyList<>();
		list.prepend(22);
		list.prepend(15);
		list.prepend(34);
		assertEquals(Integer.valueOf(34), list.get(0));
		assertEquals(Integer.valueOf(15), list.get(1));
		assertEquals(Integer.valueOf(22), list.get(2));

	}
	
	@Test
	public void testAppend(){
		MyList<Integer> list = new MyList<>();
		list.append(12);
		list.append(43);
		list.append(76);
		list.append(100);
		assertEquals(Integer.valueOf(12), list.get(0));
		assertEquals(Integer.valueOf(43), list.get(1));
		assertEquals(Integer.valueOf(76), list.get(2));
		assertEquals(Integer.valueOf(100), list.get(3));
	}
	
	@Test
	public void testAddAfterIndex(){
		MyList<Integer> list = new MyList<>();
		list.append(2);
		list.append(44);

		list.addAfterIndex(900, 0);
		list.addAfterIndex(500, 3);
		list.addAfterIndex(1000, 4);
		assertEquals(Integer.valueOf(900), list.get(0));
		assertEquals(Integer.valueOf(500), list.get(3));
		assertEquals(Integer.valueOf(1000), list.get(4));
	}
	
	@Test
	public void testRemoveElementByIndex(){
		MyList<Integer> list = new MyList<>();
		list.append(2);
		list.append(44);
		list.removeElementByIndex(0);
		assertEquals(Integer.valueOf(44), list.get(0));
	}
	
	@Test
	public void testRemoveElementByValue(){
		MyList<Integer> list = new MyList<>();
		list.append(2);
		list.append(44);
		list.append(700);
		list.append(900);
		list.append(1000);
		list.removeElementByValue(2);
		list.removeElementByValue(900);
		assertEquals(Integer.valueOf(44), list.get(0));
		assertEquals(Integer.valueOf(1000), list.get(2));
	}
	
	@Test
	public void testReplaceElement(){
		MyList<Integer> list = new MyList<>();
		list.append(2);
		list.append(66);
		list.append(800);
		list.replaceElement(1, 1000);
		assertEquals(Integer.valueOf(1000), list.get(1));
	}
	
	@Test
	public void testReversal(){
		MyList<Integer> list = new MyList<>();
		list.append(200);
		list.append(66);
		list.append(800);
		list.reversal();
		for(Integer i : list){
			System.out.println(i);
		}
		assertEquals(Integer.valueOf(200),list.get(2));
		assertEquals(Integer.valueOf(66),list.get(1));
		assertEquals(Integer.valueOf(800),list.get(0));
		
		
	}
	
//	@Test
//	public void testPrint(){
//		MyList<Integer> list = new MyList<>();
//		list.append(200);
//		list.append(66);
//		list.append(800);
//		list.append(222);
//		list.append(444);
//		for(Integer i : list){
//			System.out.println("Element " + i);
//		}
//	}

	}
