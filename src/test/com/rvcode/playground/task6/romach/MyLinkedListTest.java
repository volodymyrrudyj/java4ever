package com.rvcode.playground.task6.romach;

import com.rvcode.playground.task6.romach.MyLinkedList;
import org.junit.Before;
import org.junit.After;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class MyLinkedListTest {
    private static MyLinkedList<Integer> list = null;

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Before
    public void setUp() {
        MyLinkedListTest.list = new MyLinkedList<>(1,2,3);
    }

    @Test
    public void equalsTest(){
        assertEquals(new MyLinkedList<Integer>(1,2,3), new MyLinkedList<Integer>(1,2,3));
    }

    @Test
    public void getTest() {
        assertEquals(new Integer(1), list.get(0));
        assertEquals(new Integer(2), list.get(1));
        assertEquals(new Integer(3), list.get(2));
    }

    /**
     * 1 2 3 -> 1 2 3 4
     */
    @Test
    public void addTest() {
        list.add(4);
        assertEquals(new MyLinkedList<Integer>(1,2,3,4), list);
    }

    /**
     * 1 2 3 -> 1 4 2 3
     */
    @Test
    public void addAfterTest() {
        list.addAfter(0, 4);
        assertEquals(new MyLinkedList<Integer>(1,4,2,3), list);
    }

    /**
     * 1 2 3 -> 0 1 2 3
     */
    @Test
    public void addFirstTest() {
        list.addFirst(0);
        assertEquals(new MyLinkedList<Integer>(0,1,2,3), list);
    }

    /**
     * 1 2 3 -> 1 2 3 4 -> 2 3 4 -> 2 4 -> 2
     */
    @Test
    public void deleteIndexTest() {
        list.add(4);
        list.deleteIndex(0);
        assertEquals(new MyLinkedList<Integer>(2,3,4), list);
        list.deleteIndex(1);
        assertEquals(new MyLinkedList<Integer>(2,4), list);
        list.deleteIndex(1);
        assertEquals(new MyLinkedList<Integer>(2), list);
    }

    /**
     * 1 2 3 -> 1 2 3 1 -> 2 3
     */
    @Test
    public void deleteValuesTest(){
        list.add(1);
        list.deleteValues(1);
        assertEquals(new MyLinkedList<Integer>(2,3), list);
    }

    @Test
    public void indexOfTest(){
        assertEquals(0, list.indexOf(1));
        assertEquals(2, list.indexOf(3));
    }

    /**
     * 1 2 3 -> 4 2 3 -> 4 5 3 -> 4 5 6
     */
    @Test
    public void setTest(){
        final int listSize = 3;
        for (int i = 0; i < listSize; i++) {
            list.set(i, i + listSize + 1);
        }
        assertEquals(new MyLinkedList<Integer>(4,5,6), list);
    }

    /**
     * 1 2 3 -> 3 2 1
     */
    @Test
    public void reverseTest(){
        list.reverse();
        assertEquals(new MyLinkedList<Integer>(3,2,1), list);
    }


    /**
     * LinkedListTest.list == 1 2 3
     */
    @Test
    public void forEachTest(){
        int index = 1;
        for(Integer currentValue : MyLinkedListTest.list){
            assertEquals(new Integer(index), currentValue);
            index++;
        }
    }

    @Test
    public void indexOfBoundsExceptionTest() {
        exception.expect(IndexOutOfBoundsException.class);
        list.get(-1);
        list.get(4);
    }

    @Test
    public void notEqualsTest(){
        assertNotEquals(new MyLinkedList<Integer>(1,2,3), new MyLinkedList<Integer>(3,2,1));
    }

    @After
    public void tearDown() {
        MyLinkedListTest.list = null;
    }
}
